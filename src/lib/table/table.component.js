import styles from './table.component.scss';
import html from './table.component.html';

const template = document.createElement('template');
template.innerHTML = `
  <style>${styles.toString()}</style>
`+html;


class Table extends HTMLElement {


    constructor() {
        super();
        var self = this;


        this.content=false;
        this.reverseOrder =false;
        // Add a shadow DOM
        const shadowDOM = this.attachShadow({ mode: 'open' }); 
        
        // Render the template in the shadow dom
        shadowDOM.appendChild(template.content.cloneNode(true));


    }

    static get observedAttributes() {
        return ['content'];
    }

    // Kallas när elementet läggs till i DOMen
    connectedCallback() {
        const shadowDOM = this.shadowRoot;

        const ths = shadowDOM.querySelectorAll('th');
        for (var i=0; i<ths.length; i++) {

            var self = this;
            ths[i].onclick =  function() {   
                self.sort_table(this);
            };
        }

    }

    sort_table(el){
        var self = this;
        this.content.sort(function (a, b) {
            if(self.reverseOrder){
                return a[el.innerHTML] < b[el.innerHTML] ? 1 : -1;
            }else{
                return a[el.innerHTML] > b[el.innerHTML] ? 1 : -1;
            }
        });

        this.reverseOrder = !this.reverseOrder;

        this.renderChanges();

        
    }

    attributeChangedCallback(attr, oldVal, newVal) {
        console.log(attr);
        console.log(oldVal);
        this.content= JSON.parse(newVal);

        this.renderChanges();
      }

      get_table_headers(json_object){

        var headers = []
          if(json_object.length){
            for(var i  = 0 ; i < json_object.length ; i++){

                for (var key in json_object[i]) {
                    if(!headers.includes(key)){
                        headers.push(key);
                    }

                }

            }
          }
          return headers;
      }

      renderChanges() {


        if(this.content){
            const tbody = this.shadowRoot.querySelector('table > tbody');
            tbody.innerHTML = "";
            var headers = this.get_table_headers(this.content);




            //Sätt headers för tabellen
            var tr = document.createElement('tr');
            for(var i  = 0 ; i < headers.length ; i++){
                tr.innerHTML += '<th>'+headers[i]+'</th>';
            }
            tbody.appendChild(tr);



            //rendera innehåller i tabellen
            for(var i  = 0 ; i < this.content.length ; i++){
                var tr = document.createElement('tr');
                for(var n  = 0 ; n < headers.length ; n++){
             
                    tr.innerHTML += `<td>`+this.content[i][headers[n]]+`</td>`;
                }
                tbody.appendChild(tr);
                
            }
            this.connectedCallback();

            
        }

      }

}


export default Table;

