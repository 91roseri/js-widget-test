import styles from './button.component.scss';
import html from './button.component.html';

const template = document.createElement('template');
template.innerHTML = `
  <style>${styles.toString()}</style>
`+html;


class Button extends HTMLElement {
    constructor() {
        super();
        
        // Add a shadow DOM
        const shadowDOM = this.attachShadow({ mode: 'open' }); 
        
        // Render the template in the shadow dom
        shadowDOM.appendChild(template.content.cloneNode(true));


    }

    // Kallas när elementet läggs till i DOMen
    connectedCallback() {
        console.log('connectedCallback');
    }

    attributeChangedCallback(attr, oldVal, newVal) {
        console.log(attr);
        console.log(oldVal);
        console.log(newVal);
      }

}


export default Button;

