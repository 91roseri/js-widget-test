import Globals from './base/js/globals';

import Button from './lib/button/button.component';
import Table from './lib/table/table.component';

customElements.define(Globals.get_prefix()+'test-button', Button);
customElements.define(Globals.get_prefix()+'table', Table);


